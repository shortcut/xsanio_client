#!/usr/bin/env python

import psutil
import time
import SocketServer
import SimpleHTTPServer
import json
import logging
import multiprocessing
import os
import sys
import re
import subprocess
import ConfigParser


CONFIG_PATH = os.path.splitext(os.path.basename(__file__))[0] + '.cfg'
SAMPLE_TIME = 2.0
CVLABEL_REFRESH_TIME = 60.0
HTTP_HOST = ''
HTTP_PORT = 8090
IO_STATS = None
CVLABEL_DATA = None
CVLABEL_PATH = '/usr/sbin/cvlabel'
CVLABEL_PATTERN = r'\/dev\/r(disk\d+)\s+\[.+\]\s+acfs\s+\"(.+)\"'
LOGGER = logging.getLogger(__name__)
LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) '
              '-35s %(lineno) -5d: %(message)s')
LOG_LEVEL = logging.ERROR


def get_cmd_output(proc_args):
    proc = subprocess.Popen(proc_args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, )
    proc_output = proc.communicate()[0]
    return proc_output


def parse_config():
    """Override default values with ones from configuration file

    """
    global SAMPLE_TIME
    global CVLABEL_REFRESH_TIME
    global HTTP_PORT
    global LOG_LEVEL

    if os.path.isfile(CONFIG_PATH):
        config = ConfigParser.ConfigParser()
        try:
            LOGGER.info("Found configuration file %s, reading settings", CONFIG_PATH)
            config.read(CONFIG_PATH)

            SAMPLE_TIME = config.getfloat('Xsanio', 'sample_time')
            CVLABEL_REFRESH_TIME = config.getfloat('Xsanio', 'cvlabel_refresh_time')
            HTTP_PORT = config.getint('Xsanio', 'http_port')

            _log_level = config.get('Xsanio', 'log_level')
            if _log_level == 'ERROR':
                LOG_LEVEL = logging.ERROR
            if _log_level == 'INFO':
                LOG_LEVEL = logging.INFO

        except:
            LOGGER.error("Error parsing config file %s: %s", CONFIG_PATH, sys.exc_info()[0])


def parse_cvlabel():
    """Parse cvlabel -l output
    Present it as a dictionary

    """
    result = {}

    if os.path.exists(CVLABEL_PATH):
        cvlabel_output = get_cmd_output([CVLABEL_PATH, '-l'])
        cvlabels = re.findall(CVLABEL_PATTERN, cvlabel_output)
        for cvlabel in cvlabels:
            LOGGER.info("Found cvlabel: %s", cvlabel)
            result[cvlabel[0]] = cvlabel[1]
    else:
        LOGGER.info("This machine doesn't seem to have Xsan enabled")

    return result


class process_launcher():
    """Launches multiple PROCESSES.
    Watches if they're alive.
    If any of them exits, terminates the whole bunch

    """
    PROCESSES = []
    ALL_PROCESSES_ALIVE = True


    def add_process(self, process):
        self.PROCESSES.append(process)

    def run(self):
        LOGGER.info("Registered processes: %s" , self.PROCESSES)
        for process in self.PROCESSES:
            process.start()

        while True:
            #   Check if all processes are alive
            for process in self.PROCESSES:
                process.join(0.5)
                if not process.is_alive():
                    LOGGER.info(
                        "Process %s had unhandled exception, exiting",
                        process.name)
                    self.ALL_PROCESSES_ALIVE = False

            if not self.ALL_PROCESSES_ALIVE:
                #   Kill everyone and exit
                for process in self.PROCESSES:
                    LOGGER.info(
                        "Killing process %s",
                        process.name)
                    process.terminate()

                #   Exit the loop, terminate the program
                break


def get_io_samples(sample_time):
    """Returns the IO/s and Bytes/s statistics for a given sample time

    """
    LOGGER.info("Updating I/O statistics")
    ioSampleStart = psutil.disk_io_counters(perdisk=True)
    time.sleep(SAMPLE_TIME)
    ioSampleEnd = psutil.disk_io_counters(perdisk=True)

    ioDelta = {}
    ioStats = {}

    for disk in ioSampleEnd:
        try:
            result = {
                'io_read_delta': ioSampleEnd[disk][0] - ioSampleStart[disk][0],
                'io_write_delta': ioSampleEnd[disk][1] - ioSampleStart[disk][1],
                'bytes_read_delta': ioSampleEnd[disk][2] - ioSampleStart[disk][2],
                'bytes_write_delta': ioSampleEnd[disk][3] - ioSampleStart[disk][3],
                }
            ioDelta[disk] = result
            LOGGER.info("Delta for %s: %s", disk, result)
        except KeyError:
            LOGGER.warning('Looks like %s was unmounted', disk)

    for disk in ioDelta:
        try:
            result = {
                'io_read_sec': float(ioDelta[disk]['io_read_delta']) / float(SAMPLE_TIME),
                'io_write_sec': float(ioDelta[disk]['io_write_delta']) / float(SAMPLE_TIME),
                'bytes_read_sec': float(ioDelta[disk]['bytes_read_delta']) / float(SAMPLE_TIME),
                'bytes_write_sec': float(ioDelta[disk]['bytes_write_delta']) / float(SAMPLE_TIME),
                }
            ioStats[disk] = result
            LOGGER.info("I/O stats for %s: %s", disk, result)
        except KeyError:
            LOGGER.warning('Looks like %s was unmounted', disk)

    LOGGER.info("Statistics update complete")
    return ioStats


def get_current_io_stats():
    """Returns current I/O stats (for http handler)

    """
    global IO_STATS
    LOGGER.info("Current I/O stats: %s", IO_STATS[0])
    return IO_STATS[0]

def get_current_cvlabels():
    """Returns current cvlabels (for http handler)

    """
    global CVLABEL_DATA
    LOGGER.info("Current cvlabel data: %s", CVLABEL_DATA[0])
    return CVLABEL_DATA[0]


class custom_http_handler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    """This handler is supposed to do the following:
        1) respond with JSON-serialized I/O stats when '/' is requested
        2) respond with 404 for all other URLs

    """
    def do_GET(self):
        if self.path=='/':
            LOGGER.info("Received statistics request")
            LOGGER.info("Responding with I/O stats: %s", get_current_io_stats())
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()

            output = {
                'iodata': get_current_io_stats(),
                'cvlabel': get_current_cvlabels(),
                'sample_time': SAMPLE_TIME,
            }

            self.wfile.write(json.dumps(output))
        else:
            self.send_response(404)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.wfile.write("404")

    def log_message(self, format, *args):
        LOGGER.info(*args)

def update_io_stats():
    """launches the I/O stats update process.
    Does the update every SAMPLE_TIME seconds.
    Stores the results in IO_STATS list, first element

    """
    LOGGER.info("Launching I/O stat updater")
    global IO_STATS
    while True:
        LOGGER.info("Current I/O stats: %s", IO_STATS)
        IO_STATS[0] = get_io_samples(SAMPLE_TIME)
        LOGGER.info("Updated I/O stats: %s", IO_STATS)

def update_cvlabels():
    """Update the cvlabels once in a while

    """
    LOGGER.info("Launching cvlabel data updater")
    global CVLABEL_DATA
    while True:
        LOGGER.info("Updating cvlabels")
        CVLABEL_DATA[0] = parse_cvlabel()
        time.sleep(CVLABEL_REFRESH_TIME)

def run_web_server():
    """Launches the web server process

    """
    try:
        http_server = SocketServer.ThreadingTCPServer((HTTP_HOST, HTTP_PORT), custom_http_handler)
        LOGGER.info("Starting HTTP server on port %s", HTTP_PORT)
        http_server.serve_forever()
    except KeyboardInterrupt:
        http_server.shutdown()
    except:
        LOGGER.error("HTTP server error: %s", sys.exc_info()[0])


def main():
    """Run a web server process and I/O stats update process

    """
    global IO_STATS
    global CVLABEL_DATA
    logging.basicConfig(level=LOG_LEVEL, format=LOG_FORMAT)

    if not os.geteuid() == 0:
        sys.exit('Must be run as root user')

    parse_config()

    #   proc_launcher will handle the multiple processes
    proc_launcher = process_launcher()

    #   Setting up the IO_STATS list which will contain only one item: the current I/O stats
    #   It is created as Manager List, so is accessible from all processes
    #   It's not created as a dictionary, since the dicts created by Manager are not JSON-serializable
    #   (well, actually they are, but json module doesn't know how to handle them)
    proc_manager = multiprocessing.Manager()
    IO_STATS = proc_manager.list()
    IO_STATS.append({})

    #   Same for CVLABEL_DATA
    CVLABEL_DATA = proc_manager.list()
    CVLABEL_DATA.append({})

    proc_launcher.add_process(multiprocessing.Process(
        target=update_cvlabels,
        name='update_cvlabels'
    ))
    proc_launcher.add_process(multiprocessing.Process(
        target=update_io_stats,
        name='update_io_stats'
    ))
    proc_launcher.add_process(multiprocessing.Process(
        target=run_web_server,
        name='run_web_server'
    ))

    #   Join forever
    proc_launcher.run()


if __name__ == "__main__":
    main()
